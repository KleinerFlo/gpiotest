#!/usr/bin/env python3

from snipsTools import SnipsConfigParser
from hermes_python.hermes import Hermes

# imported to get type check and IDE completion
from hermes_python.ontology.dialogue.intent import IntentMessage
import RPi.GPIO as GPIO


# GPIO-Pins festlegen

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)


class GPIOTEST:
    """class used to wrap action code with mqtt connection
       please change the name referring to your application
    """

    def __init__(self):
        # get the configuration if needed

        self.config = None

        # start listening to MQTT
        self.start_blocking()

    @staticmethod
    def Licht_an_callback(hermes: Hermes,
                          intent_message: IntentMessage):
        result_sentence = "Ich habe leider nicht verstanden, welches Licht du meinst."
        if (len(intent_message.slots) != 0):
            farbe = intent_message.slots.Farbe.first().value # Extrahieren des Slots "Farbe"
            if (farbe == 'Blau' or farbe == 'Blaue' or farbe == 'Blaues'):
                GPIO.output(17,True)
                result_sentence = "Schalte ein : blaue LED"  # Antwortsatz den die TTS Engine vorliest.
            elif (farbe == 'Gruen' or farbe == 'Grün' or farbe == 'Grünes' or farbe == 'Grüne' or farbe == 'Gruenes' or farbe == 'Gruene'):
                GPIO.output(27,True) # GPIO einschalten
                result_sentence = "Schalte ein : grüne LED"  
            elif (farbe == 'Orange' or farbe == 'Gelb' or farbe == 'Orangenes' or farbe == 'Gelbes' or farbe == 'Orangene' or farbe == 'Gelbe'):
                GPIO.output(22,True)
                result_sentence = "Schalte ein : orangene LED"  
        else:
            result_sentence = "Das Licht wird eingeschaltet"
            GPIO.output(17,True)
            GPIO.output(27,True)
            GPIO.output(22,True)
        # Session beenden und Satz an TTS Engine senden
        hermes.publish_end_session(intent_message.session_id, result_sentence)

    @staticmethod
    def Licht_aus_callback(hermes: Hermes,
                          intent_message: IntentMessage):
        result_sentence = "Ich habe leider nicht verstanden, welches Licht du meinst."
        if (len(intent_message.slots) != 0):
            farbe = intent_message.slots.Farbe.first().value # Extrahieren des Slots "Farbe"
            if (farbe == 'Blau' or farbe == 'Blaue' or farbe == 'Blaues'):
                GPIO.output(17,False)
                result_sentence = "Schalte aus : blaue LED"   # Antwortsatz den die TTS Engine vorliest.
            elif (farbe == 'Gruen' or farbe == 'Grün' or farbe == 'Grünes' or farbe == 'Grüne' or farbe == 'Gruenes' or farbe == 'Gruene'):
                GPIO.output(27,False) # GPIO ausschalten
                result_sentence = "Schalte aus : grüne LED"  
            elif (farbe == 'Orange' or farbe == 'Gelb' or farbe == 'Orangenes' or farbe == 'Gelbes' or farbe == 'Orangene' or farbe == 'Gelbe'):
                GPIO.output(22,False)
                result_sentence = "Schalte aus : orangene LED"  
        else:
            result_sentence = "Das Licht wird ausgeschaltet"
            GPIO.output(17,False)
            GPIO.output(27,False)
            GPIO.output(22,False)
        # Session beenden und Satz an TTS Engine senden 
        hermes.publish_end_session(intent_message.session_id, result_sentence)


    # Den Intents die richtige Callback Funktion zuweisen und MQTT Verbindung aufbauen
    def start_blocking(self):
        with Hermes("localhost:1883") as h:
            h.subscribe_intent('FloD:LichtAn', self.Licht_an_callback)\
            .subscribe_intent('FloD:LichtAus', self.Licht_aus_callback)\
            .loop_forever()


if __name__ == "__main__":
    GPIOTEST()
